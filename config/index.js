const path = require('path');

const outputPath = `demos/${process.env.TARO_ENV}`;

const config = {
  projectName: 'taro-mall',
  date: '2020-7-1',
  designWidth: 750,
  deviceRatio: {
    '640': 2.34 / 2,
    '750': 1,
    '828': 1.81 / 2
  },
  sourceRoot: 'src',
  outputRoot: outputPath,
  babel: {
    sourceMap: true,
    presets: [
      ['env', {
        modules: false
      }]
    ],
    plugins: [
      'transform-decorators-legacy',
      'transform-class-properties',
      'transform-object-rest-spread',
      ['transform-runtime', {
        "helpers": false,
        "polyfill": false,
        "regenerator": true,
        "moduleName": 'babel-runtime'
      }]
    ]
  },
  copy: {
    patterns: [
      // { from: 'ext.json', to: `${outputRoot}/ext.json` } // 第三方配置
    ],
    options: {}
  },
  alias: {
    '@/src': path.resolve(__dirname, '..', 'src'),
    // '@/api': path.resolve(__dirname, '..', 'src/api'),
    // '@/utils': path.resolve(__dirname, '..', 'src/utils'),
    // '@/store': path.resolve(__dirname, '..', 'src/store'),
    // '@/pages': path.resolve(__dirname, '..', 'src/pages'),
    // '@/assets': path.resolve(__dirname, '..', 'src/assets'),
    // '@/plugins': path.resolve(__dirname, '..', 'src/plugins'),
    // '@/services': path.resolve(__dirname, '..', 'src/services'),
    // '@/components': path.resolve(__dirname, '..', 'src/components'),
  },
  plugins: [
    '@tarojs/plugin-sass',
    '@tarojs/plugin-terser',
  ],
  defineConstants: {
  },
  mini: {
    postcss: {
      pxtransform: {
        enable: true,
        config: {}
      },
      url: {
        enable: true,
        config: {
          limit: 10240 // 设定转换尺寸上限
        }
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: 'module', // 转换模式，取值为 global/module
          generateScopedName: '[name]__[local]___[hash:base64:5]'
        }
      }
    }
  },
  h5: {
    publicPath: '/',
    staticDirectory: 'static',
    postcss: {
      autoprefixer: {
        enable: true,
        config: {
          browsers: [
            'last 3 versions',
            'Android >= 4.1',
            'ios >= 8'
          ]
        }
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: 'module', // 转换模式，取值为 global/module
          generateScopedName: '[name]__[local]___[hash:base64:5]'
        }
      }
    },
    esnextModules: ['taro-ui', 'taro-skeleton']
  }
}

module.exports = function (merge) {
  if (process.env.NODE_ENV === 'development') {
    return merge({}, config, require('./dev'))
  }
  return merge({}, config, require('./prod'))
}
